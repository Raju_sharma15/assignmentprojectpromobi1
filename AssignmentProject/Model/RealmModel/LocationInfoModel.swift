//
//  LocationInfoModel.swift
//  AssignmentProject
//
//  Created by webwerks on 09/07/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class LocationInfoModel : Object {
        @objc dynamic var latitude : Data?
        @objc dynamic var longitude  : Data?
        @objc dynamic var date : Data?
}
