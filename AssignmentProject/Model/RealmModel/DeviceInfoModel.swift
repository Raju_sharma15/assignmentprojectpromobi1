//
//  DeviceInfoModel.swift
//  AssignmentProject
//
//  Created by webwerks on 09/07/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class DeviceInfoModel: Object {
    @objc dynamic var date : Data?
    @objc dynamic var cpuUsage  : Data?
    @objc dynamic var memoryUsage :  Data?
    @objc dynamic var batteryPercentage :   Data?
    @objc dynamic var batteryState :  Data?
    @objc dynamic var batteryLevel :   Data?
    
    
    class  func getRecordWith(query : String) -> Results<DeviceInfoModel>? {
        return RealmHelper.queryForObjects(type: DeviceInfoModel.self, query: query)
        
    }
}



