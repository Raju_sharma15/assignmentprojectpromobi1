//
//  CustomTableViewCell.swift
//  AssignmentProject
//
//  Created by webwerks on 10/07/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var batteryPerLBL: UILabel!
    @IBOutlet weak var batteyStateLBL: UILabel!
    @IBOutlet weak var batterLevelLBL: UILabel!
    @IBOutlet weak var memoryLBL: UILabel!
    @IBOutlet weak var cpuLBL: UILabel!
    @IBOutlet weak var dateLBL: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
