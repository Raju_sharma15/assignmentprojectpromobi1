
import Foundation
import CoreLocation
import UIKit

class BackgroundLocationManager :NSObject, CLLocationManagerDelegate {
    
    static let instance = BackgroundLocationManager()
    static let BACKGROUND_TIMER = 60 // restart location manager every 60 seconds
    static let UPDATE_SERVER_INTERVAL = 60
    
    let locationManager = CLLocationManager()
    let anotherLocationManager =  CLLocationManager()
    var timer:Timer?
    var currentBgTaskId : UIBackgroundTaskIdentifier?
    var lastLocationDate : NSDate = NSDate()
    var currentLocation : CLLocation?
    var locationClouser : ((_ location : CLLocation)->Void)? = nil
    var  myLocationAccuracy : CLLocationAccuracy?
    
       override init(){
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.activityType = .other
        locationManager.distanceFilter = kCLDistanceFilterNone
        if #available(iOS 9, *){
            locationManager.allowsBackgroundLocationUpdates = true
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    @objc func applicationEnterBackground(){
        start()
    }
      
    func start(){
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            if #available(iOS 9, *){
                locationManager.requestLocation()
            } else {
             //   locationManager.startUpdatingLocation()
                 locationManager.startMonitoringSignificantLocationChanges()
            }
        } else {
            locationManager.requestAlwaysAuthorization()
        }

    }
    @objc func restart (){
        timer?.invalidate()
        timer = nil
        start()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case CLAuthorizationStatus.restricted: break
        case CLAuthorizationStatus.denied: break
        case CLAuthorizationStatus.notDetermined: break
        default:
            if #available(iOS 9, *){
                locationManager.requestLocation()
            } else {
               // locationManager.startUpdatingLocation()
                locationManager.startMonitoringSignificantLocationChanges()
            }
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(timer==nil){
            guard locations.last != nil else {return}
            guard let manage =  manager.location?.coordinate else{
                return
            }
            let locationValue : CLLocationCoordinate2D = manage
            self.currentLocation = CLLocation(latitude: locationValue.latitude, longitude: locationValue.longitude)
            self.myLocationAccuracy = self.currentLocation?.horizontalAccuracy
            beginNewBackgroundTask()
            let now = NSDate()
            if(isItTime(now: now)){
                self.locationClouser!(self.currentLocation!)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        beginNewBackgroundTask()
        locationManager.stopMonitoringSignificantLocationChanges()
//        locationManager.stopUpdatingLocation()
    }
    
    func isItTime(now:NSDate) -> Bool {
        let timePast = now.timeIntervalSince(lastLocationDate as Date)
        let intervalExceeded = Int(timePast) > BackgroundLocationManager.UPDATE_SERVER_INTERVAL
        return intervalExceeded;
    }
    
    func beginNewBackgroundTask(){
        var previousTaskId = currentBgTaskId;
        currentBgTaskId = UIApplication.shared.beginBackgroundTask(expirationHandler: {
        })
        if let taskId = previousTaskId{
            UIApplication.shared.endBackgroundTask(taskId)
            previousTaskId = UIBackgroundTaskIdentifier.invalid
        }
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(BackgroundLocationManager.BACKGROUND_TIMER), target: self, selector: #selector(self.restart),userInfo: nil, repeats: false)
    }
}
