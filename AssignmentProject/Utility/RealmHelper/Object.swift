
import RealmSwift
// MARK: - This is an extension for Realm type object
extension Object {
    /// allows to save object to realmdb




    func updateRecord(completion : ((_ status : Bool? )->())) {
        do{
            let realm = try Realm()
            try realm.write {
                realm.add(self, update: true)
            }
            completion(true)
        }
        catch{
            print("Save error : \(error.localizedDescription)")
        }
    }

    func saveobject(completion : ((_ status : Bool? )->())) {
        do{
            let realm = try Realm()
            try realm.write {
                realm.add(self)
            }
            completion(true)
        }
        catch{
            print("Save error : \(error.localizedDescription)")
        }
    }


    func update(_ updateBlock: () -> ()) {
        do{
            let realm = try Realm()
            try realm.write(updateBlock)
        }
        catch{
            print("Update error : \(error.localizedDescription)")
        }
    }


    /// delete current object
    func delete()  {
        do{
            let realm = try Realm()
            try realm.write {
                realm.delete(self)
            }
        }
        catch{
            print("Delete error : \(error.localizedDescription)")
        }
    }
}




extension Results {
    /// Converts Result<> to [T]
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for result in self {
            if let result = result as? T {
                array.append(result)
            }
        }
        return array
    }
}

extension List {
    /// Converts List<> to [T]
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for result in self {
            if let result = result as? T {
                array.append(result)
            }
        }
        return array
    }
}

class IntObject: Object {
    @objc dynamic var value = 0
}

class StringObject: Object {
    @objc dynamic var value = ""
    override class func primaryKey() -> String? {
        return "value"
    }
}

extension String {
    /// Converts String to StringObject
    func toStringObject() -> StringObject {
        return StringObject(value: self)
    }
}

extension Sequence where Iterator.Element == String {
    /// Converts Result<StringObject> to List<StringObject>
    func toStringObjects() -> List<StringObject> {
        let list = List<StringObject>()
        for s in self {
            list.append(s.toStringObject())
        }
        return list
    }
}

extension Int {
    /// Converts Int to IntObject
    func toIntObject() -> IntObject {
        return IntObject(value: self)
    }
}

extension Sequence where Iterator.Element == Int {
    /// Converts Result<IntObject> to List<IntObject>
    func toIntObjects() -> List<IntObject> {
        let list = List<IntObject>()
        for s in self {
            list.append(s.toIntObject())
        }
        return list
    }
}
