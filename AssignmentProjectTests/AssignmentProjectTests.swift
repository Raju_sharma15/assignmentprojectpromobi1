//
//  AssignmentProjectTests.swift
//  AssignmentProjectTests
//
//  Created by webwerks on 08/07/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import XCTest
@testable import AssignmentProject

class AssignmentProjectTests: XCTestCase {
//    var locationModelArray = [LocationInfoModel]()
//    var deviceInfoModelArray = [DeviceInfoModel]()
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    // MARK: - Positive case
    
    func testPostiveCpuUsageAndBattery(){
        let usage  =    CpuInfoClass.Sharedinstance.cpuUsage()
        XCTAssertGreaterThan(usage, -1)
        let memoryUsage =   CpuInfoClass.Sharedinstance.memoryUsage()
        XCTAssertGreaterThan(memoryUsage, "")
        let batteryInfo = BatteryInfoClass.SharedInstancebattery
        batteryInfo.batteryLevelClouser = { batteryLevel in
            XCTAssertGreaterThan(batteryLevel, -1)
        }
        batteryInfo.batteryStateClouser = { batteryState in
            XCTAssertGreaterThan(Int(Float(batteryState.hashValue)), -1)
        }
        batteryInfo.batteryPercentageClouser = { batteryPercentage in
            // samultor -100 
            XCTAssertGreaterThan(batteryPercentage, -101)
        }
        batteryInfo.startBatteryInfo()
    }
   
    // MARK: - Nagetive case
    
    func testNagetiveCpuUsageAndBattery() {
        let usage  =    CpuInfoClass.Sharedinstance.cpuUsage()
        XCTAssertLessThan(usage, -1)
        let memoryUsage =   CpuInfoClass.Sharedinstance.memoryUsage()
        let memoryUsageValue = memoryUsage.substring(to:memoryUsage.index(memoryUsage.startIndex, offsetBy: 1))
        XCTAssertLessThan(Double(memoryUsageValue)! , -1)
        let batteryInfo = BatteryInfoClass.SharedInstancebattery
        batteryInfo.batteryLevelClouser = { batteryLevel in
            XCTAssertLessThan(batteryLevel, -1)
        }
        batteryInfo.batteryStateClouser = { batteryState in
            XCTAssertLessThan(Int(Float(batteryState.hashValue)), -1)
        }
        batteryInfo.batteryPercentageClouser = { batteryPercentage in
            XCTAssertLessThan(batteryPercentage, -1)
        }
        batteryInfo.startBatteryInfo()
    }
    }



